module.exports = {
    $schema: "https://docs.renovatebot.com/renovate-schema.json",
    description: ["Base preset for use with KoiReader's repos"],
    platform: 'gitlab',
    endpoint: 'https://gitlab.com/api/v4/',
    token: process.env.RENOVATE_TOKEN,
    repositories: [
        'NishantTyagi/random-test',
    ],
    enabled: true,
    gitLabAutomerge: true,
    prCreation: "status-success",


    commitMessagePrefix: "[renovate] ",
    npmToken: "<redacted>",
    rangeStrategy: "bump",
    logLevel: 'info',

    requireConfig: true,
    onboarding: true,
    onboardingConfig: {
        extends: ["config:base",
            ":pinAllExceptPeerDependencies",
            ":separateMultipleMajorReleases",
            ":combinePatchMinorReleases",
            ":unpublishSafe",
            ":automergeLinters",
            ":automergeTesters",
            ":enableVulnerabilityAlerts",
            ":semanticCommits",
            ":dependencyDashboard",
            "group:allNonMajor",
            "group:allApollographql",
            "group:fortawesome",
            "group:polymer",
            "helpers:disableTypesNodeMajor",
            "packages:apollographql",
            "packages:linters"
        ],
        prConcurrentLimit: 5,
    },

    enabledManagers: [
        'npm',
    ],
    packageRules: [
        {
            datasources: ["npm"],
            stabilityDays: 7
        },
        {
            datasources: ["npm"],
            packageNames: ["aws-sdk"],
            schedule: ["after 12pm on sunday"]
        }
    ]

};